#include <WiFi.h>
#include "T1Robot.h"

#define PWM_FREQ 7500
#define PWM_RESOLUTION 8

#define M1F_PIN 12
#define M1R_PIN 13
#define M2F_PIN 15
#define M2R_PIN 14

#define M1F 0
#define M1R 1
#define M2F 2
#define M2R 4

T1Robot robot;
float angle = 0;

void setup() {
  pinMode(M1F_PIN, OUTPUT);
  pinMode(M1R_PIN, OUTPUT);
  pinMode(M2F_PIN, OUTPUT);
  pinMode(M2R_PIN, OUTPUT);
  pinMode(4, OUTPUT);

  ledcSetup(M1F, PWM_FREQ, PWM_RESOLUTION);
  ledcSetup(M1R, PWM_FREQ, PWM_RESOLUTION);
  ledcSetup(M2F, PWM_FREQ, PWM_RESOLUTION);
  ledcSetup(M2R, PWM_FREQ, PWM_RESOLUTION);

  ledcAttachPin(M1F_PIN, M1F);
  ledcAttachPin(M1R_PIN, M1R);
  ledcAttachPin(M2F_PIN, M2F);
  ledcAttachPin(M2R_PIN, M2R);

  Serial.begin(115200);
  Serial.setDebugOutput(true);

#ifndef WIFI_AP_MODE
  WiFi.begin(SSID, PASSWORD);
  WiFi.setSleep(false);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");


  Serial.print("Camera Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");

#else
  WiFi.mode(WIFI_AP);
  WiFi.softAP(SSID, PASSWORD);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
#endif


  robot.begin();

  digitalWrite(4, HIGH);
  delay(100);
  digitalWrite(4, LOW);
}



void loop() {
  int wifiStatus = WiFi.status();
  if (wifiStatus == WL_DISCONNECTED || wifiStatus == WL_CONNECTION_LOST) {
    Serial.println("Connection lost restarting");
    ESP.restart();
  }

  robot.update();

  int left = robot.left;
  int right = robot.right;
  

  if (left >= 0) {
    ledcWrite(M1F, left);
    ledcWrite(M1R, 0);
  } else {
    ledcWrite(M1F, 0);
    ledcWrite(M1R, -left);
  }

  if (right >= 0) {
    ledcWrite(M2F, right);
    ledcWrite(M2R, 0);
  } else {
    ledcWrite(M2F, 0);
    ledcWrite(M2R, -right);
  }

  vTaskDelay(10 / portTICK_PERIOD_MS);
}
