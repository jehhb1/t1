#pragma once
#include "config.h"
#include <esp_http_server.h>

#ifdef T1_RC
enum RCState {
  RC_IDLE, RC_MOVE,  RC_FORWARD, RC_CALIBRATE
};

struct RCConfig {
  int timestamp;
  RCState state;
  int throttle;
  int steerBias;
  int magnitude;
  int angle;
};
#endif

enum T1RobotMode {
  T1_RC_MODE, T1_SUMO_MODE, T1_LINE_MODE
};

class T1Robot {
private:
  httpd_handle_t server = NULL;
  T1RobotMode currentMode;


#ifdef T1_RC
  void update_rc();
#endif

public:

#if defined (T1_RC) || defined (T1_ROBOT)
  int left, right;
#endif

  T1Robot();

#ifdef T1_RC
  RCConfig rcConfig;
#endif

#ifdef T1_PASSWORD
  char currentSession[8];
#endif

  void begin();
  void update();
};