#include "esp32-hal.h"
#include "esp_err.h"
#include "config.h"
#include <string.h>
#include <stdlib.h>
#include "T1Robot.h"
#include <esp_http_server.h>
#include <Arduino.h>

#define SESSION_ALPHABET "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890-=!@#$%^&*()_+[]{}|;':,./<>?`~"
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

esp_err_t handle_cors_options(httpd_req_t *req) {
  httpd_resp_set_status(req, "204 No Content");
  httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
  httpd_resp_set_hdr(req, "Access-Control-Allow-Methods", "GET, POST, OPTIONS");
  httpd_resp_set_hdr(req, "Access-Control-Allow-Headers", "Content-Type, X-T1-Auth");
  httpd_resp_set_hdr(req, "Access-Control-Max-Age", "3600");
  httpd_resp_send(req, "", 0);
  return ESP_OK;
}

esp_err_t connect_handler(httpd_req_t *req) {
#ifdef T1_PASSWORD
  char buf[100];
  int ret, remaining = req->content_len;

  if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0) {
    return ESP_FAIL;
  }

  buf[ret] = '\0';


  if (strcmp(buf, T1_PASSWORD) == 0) {
    randomSeed(millis());

    char output[] = "{\"s\":0,\"t\":\"12345678\"}";
    char *sess = ((T1Robot *)req->user_ctx)->currentSession;

    for (int i = 0; i < 8; ++i) {
      int c = random(0, strlen(SESSION_ALPHABET));
      output[12 + i] = sess[i] = SESSION_ALPHABET[c];
    }

    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
    httpd_resp_send(req, output, 22);
  } else {
    // If password does not match, send response with status 1
    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
    httpd_resp_send(req, "{\"s\":1}", 7);
  }
#else
  httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
  httpd_resp_send(req, "{\"s\":0,\"t\":\"\"}", 14);
#endif

  return ESP_OK;
}

int authorize(httpd_req_t *req) {
#ifdef T1_PASSWORD
  char auth_header[10];
  httpd_req_get_hdr_value_str(req, "X-T1-Auth", auth_header, 10);

  const T1Robot *robot = (T1Robot *)req->user_ctx;
  if (strncmp(robot->currentSession, auth_header, 8)) {
    httpd_resp_send_err(req, HTTPD_401_UNAUTHORIZED, "");
    return 1;
  }
#endif
  return 0;
}

esp_err_t config_handler(httpd_req_t *req) {
  if (authorize(req)) return ESP_OK;
  httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");

#if defined(T1_RC)
  httpd_resp_send(req, "{\"rc\":true}", 11);
#else
  httpd_resp_send(req, "{}", 2);
#endif
  return ESP_OK;
}

#ifdef T1_RC
esp_err_t rc_handler(httpd_req_t *req) {
  if (authorize(req)) return ESP_OK;

  char buf[8];
  int ret, remaining = req->content_len;

  if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0) {
    return ESP_FAIL;
  }


  T1Robot *bot = (T1Robot *)req->user_ctx;

  vTaskDelay(10 / portTICK_PERIOD_MS);

  switch (buf[0]) {
    case 0:  // JOYSTICK
      bot->rcConfig.timestamp = millis();
      bot->rcConfig.state = RC_MOVE;
      bot->rcConfig.magnitude = buf[1];
      bot->rcConfig.angle = buf[2] * 360 / 256;
      break;
    case 1:  // CONFIG UPDATE
      bot->rcConfig.steerBias = buf[1];
      bot->rcConfig.throttle = buf[2];
      break;
    case 2:  // CALIBRATING
      bot->rcConfig.timestamp = millis();
      bot->rcConfig.state = RC_CALIBRATE;
      break;
    case 3:  // POWER FORWARD
      bot->rcConfig.timestamp = millis();
      bot->rcConfig.state = RC_FORWARD;
      break;
    default:
      bot->rcConfig.timestamp = millis();
      bot->rcConfig.state = RC_MOVE;
      bot->rcConfig.magnitude = 0;
      break;
  }


  httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
  httpd_resp_send(req, "", 0);

  return ESP_OK;
}
#endif

void T1Robot::begin() {
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.max_uri_handlers = 16;
  config.uri_match_fn = httpd_uri_match_wildcard;

  httpd_uri_t cors_options = {
    .uri = "*",
    .method = HTTP_OPTIONS,
    .handler = handle_cors_options,
  };

  httpd_uri_t connect_uri = {
    .uri = "/connect",
    .method = HTTP_POST,
    .handler = connect_handler,
    .user_ctx = this,
  };

  httpd_uri_t config_uri = {
    .uri = "/config",
    .method = HTTP_GET,
    .handler = config_handler,
    .user_ctx = this,
  };

#ifdef T1_RC
  httpd_uri_t rc_uri = {
    .uri = "/rc",
    .method = HTTP_POST,
    .handler = rc_handler,
    .user_ctx = this,
  };
#endif

  if (httpd_start(&server, &config) == ESP_OK) {
    httpd_register_uri_handler(server, &cors_options);
    httpd_register_uri_handler(server, &connect_uri);
    httpd_register_uri_handler(server, &config_uri);

#ifdef T1_RC
    httpd_register_uri_handler(server, &rc_uri);
#endif
  }
}

#ifdef T1_RC
static void lrBias(int *left, int *right, int bias) {
  if (bias <= 127) {
    *left = *left * bias / 127;
  } else {
    *right = *right * (127 - bias) / 127;
  }
}

void T1Robot::update_rc() {
  int current = millis();

  if (rcConfig.state == RC_CALIBRATE && current - rcConfig.timestamp >= 2500
      || rcConfig.state != RC_CALIBRATE && rcConfig.state != RC_IDLE && current - rcConfig.timestamp >= FALLOFF) {
    rcConfig.state = RC_IDLE;
  }

  int angle1 = HARDSTEER_ANGLE / 2;
  int angle2 = 180 - HARDSTEER_ANGLE / 2;
  int angle3 = 180 + HARDSTEER_ANGLE / 2;
  int angle4 = 360 - HARDSTEER_ANGLE / 2;
  int angle = rcConfig.angle;


  switch (rcConfig.state) {
    case RC_MOVE:
      if (angle < angle1 || angle > angle4) {
        left = 255;
        right = -255;
      } else if (angle > angle2 && angle < angle3) {
        left = -255;
        right = 255;
      } else if (angle >= angle1 && angle <= angle2) {
        if (angle <= 90) {
          left = 255;
          right = 255 * (angle - angle1) / (90 - angle1);
        } else {
          left = 255 - (255 * (angle - 90) / (angle2 - 90));
          right = 255;
        }
      } else {
        if (angle >= 270) {
          left = -255;
          right = -255 + (255 * (angle - 270) / (angle4 - 270));
        } else {
          left = -255 * (angle - angle3) / (270 - angle3);
          right = -255;
        }
      }

      lrBias(&left, &right, rcConfig.steerBias);
      left = left * rcConfig.throttle * rcConfig.magnitude / (255 * 255);
      right = right * rcConfig.throttle * rcConfig.magnitude / (255 * 255);

      Serial.print("MOVE\t");
      Serial.print(left);
      Serial.print('\t');
      Serial.print(right);

      break;
    case RC_FORWARD:
      left = right = 255;
      Serial.print("FORW\t");
      Serial.print(left);
      Serial.print('\t');
      Serial.print(right);
      break;
    case RC_CALIBRATE:
      left = right = 255;
      lrBias(&left, &right, rcConfig.steerBias);

      Serial.print("CALB\t");
      Serial.print(left);
      Serial.print('\t');
      Serial.print(right);
      break;
    default:
    case RC_IDLE:
      left = right = 0;
      Serial.print("IDLE\t");
      Serial.print(left);
      Serial.print('\t');
      Serial.print(right);
      break;
  }
  Serial.println();
}
#endif

void T1Robot::update() {
  #ifdef T1_RC
    if (currentMode == T1_RC_MODE) update_rc();
  #endif
}

T1Robot::T1Robot() {
#ifdef T1_RC
  rcConfig.angle = 0;
  rcConfig.magnitude = 0;
  rcConfig.state = RC_IDLE;
  rcConfig.timestamp = millis();
  rcConfig.steerBias = 0;
  rcConfig.throttle = 1;
  currentMode = T1_RC_MODE;
#endif
}