#define MAX_WORD_LENGTH 16
#define DEFAULT_WORDS_COUNT 100

const char DEFAULT_WORDS[DEFAULT_WORDS_COUNT][MAX_WORD_LENGTH] = {
  "bahay", "sapatos", "telepono", "aso", "pusa", "pagkain", "puno", "tubig",
  "ulan", "init", "lamig", "hangin", "kabayo", "isda", "karne", "gatas",
  "gulay", "prutas", "balita", "libro", "kompyuter", "barko", "eroplano",
  "bisikleta", "kotse", "motor", "jeep", "bus", "kalye", "kalsada", "pulis",
  "bumbero", "hospital", "doktor", "nars", "guro", "eskwela", "bata",
  "matanda", "binata", "dalaga", "kaibigan", "kapatid", "tatay", "nanay",
  "lolo", "lola", "anak", "daga", "ibon", "butterfly", "buwan", "araw",
  "bituin", "lupa", "dagat", "bundok", "ilog", "sapa", "lawa", "kubo",
  "kusina", "sala", "kwarto", "banyo", "bintana", "pintuan", "hagdan",
  "mesa", "upuan", "kama", "orasan", "alarma", "bumbilya", "kandila",
  "kabinet", "aparador", "plato", "kutsara", "tinidor", "kutsilyo", "baso",
  "takure", "kaldero", "kawali", "sandok", "palayok", "banga", "plangana",
  "tabo", "timbangan", "plantsa", "kutsarita", "kalendaryo", "payong",
  "kwintas", "singsing", "hikaw", "pulseras", "relos"
};