#pragma once
#include "HenyoWords.h"
#define WORD_COUNT 32

enum HenyoStateEnum {
  HENYO_IDLE,
  HENYO_ACTIVE,
  HENYO_PAUSED,
};

class HenyoState {
private:
  void nextWord();
public:
  HenyoStateEnum state;
  const char *words[WORD_COUNT];
  int wordCount;
  int currentWord;
  int time;
  int score;
  int lastUpdate;

  HenyoState()
    : state(HENYO_IDLE),
      currentWord(0),
      time(0),
      score(0) {
    sampleFromDefault();
  }

  void sampleFromDefault();

  const char *getState() const;

  void start(int time);
  void pause();
  void reset();
  void next();
  void check();
  void update();

};