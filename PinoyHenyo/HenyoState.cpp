#include "core_esp8266_features.h"
#include <exception>
#include "HenyoState.h"
#include <Arduino.h>

void HenyoState::nextWord() {
  if (wordCount == 0) currentWord = 0;
  else currentWord = (currentWord + 1) % wordCount;
}

void HenyoState::sampleFromDefault() {
  for (int i = 0; i < WORD_COUNT; ++i) {
    int j = random(DEFAULT_WORDS_COUNT);
    words[i] = DEFAULT_WORDS[j];
  }
  wordCount = WORD_COUNT;
}

const char* HenyoState::getState() const {
  if (state == HENYO_IDLE) return "{\"s\":0}";

  // {"s":1,"t":12345678,"sc":1234,"w":"0123456789ABCDEF"}
  const char t[64] = "{\"s\": ,\"t\":        ,\"sc\":    ,\"w\":\"";

  static char out[64];
  for(int i = 0; i < 64; ++i) {
    out[i] = t[i];
    if (t[i] == '\0') break;
  }


  out[5] = state == HENYO_ACTIVE ? '1' : '2';

  if (time > 0) {
    for (int i = 0, t = time; t > 0 && i < 8; ++i, t /= 10) {
      out[18 - i] = '0' + (t % 10);
    }
  } else {
    out[18] = '0';
  }

  if (score > 0) {
    for (int i = 0, s = score; s > 0 && i < 4; ++i, s /= 10) {
      out[28 - i] = '0' + (s % 10);
    }
  } else {
    out[28] = '0';
  }

  int j;
  for (j = 0; 35 + j < 61 && words[currentWord][j] != 0; ++j) { 
    out[35 + j] = words[currentWord][j];
  }
  out[35 + j] = '"';
  out[35 + j + 1] = '}';
  out[35 + j + 2] = '\0';
  return out;
}

void HenyoState::start(int time) {
  this->time = time;
  state = HENYO_ACTIVE;
  score = 0;
  currentWord = 0;
  lastUpdate = millis();
}

void HenyoState::pause() {
  state = state == HENYO_ACTIVE ? HENYO_PAUSED : HENYO_ACTIVE;
}

void HenyoState::reset() {
  state = HENYO_IDLE;
}

void HenyoState::next() {
  nextWord();
}

void HenyoState::check() {
  nextWord();
  ++score;
}

void HenyoState::update() {
  if (state != HENYO_ACTIVE) return;

  int current = millis();
  if (current - lastUpdate > 1000) {
    lastUpdate = current;

    if (time <= 0) {
      state = HENYO_PAUSED;
    } else {
      --time;
    }
  }
}