#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <LiquidCrystal_I2C.h>
#include "HenyoState.h"

#define SOFTAP
#define SSID "Pinoy Henyo"
#define PASSWORD "PASSWORD123"

AsyncWebServer server(80);
HenyoState henyoState;
LiquidCrystal_I2C lcd(0x27, 16, 2);

bool connected;
IPAddress ip;

void cors_handler(AsyncWebServerRequest *req) {
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  resp->addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
  resp->addHeader("Access-Control-Allow-Headers", "Content-Type, X-T1-Auth");
  resp->addHeader("Access-Control-Max-Age", "3600");
  req->send(resp);
}

void connect_handler(AsyncWebServerRequest *req) {
  AsyncWebServerResponse *resp = req->beginResponse(200, "application/json", "{\"s\":0, \"t\":\"\"}");
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void config_handler(AsyncWebServerRequest *req) {
  AsyncWebServerResponse *resp = req->beginResponse(200, "application/json", "{\"henyo\": true}");
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void words_get_handler(AsyncWebServerRequest *req) {
  char words[WORD_COUNT * MAX_WORD_LENGTH];

  int l = 0;
  for (int i = 0; i < henyoState.wordCount; ++i) {
    for (int j = 0; j < MAX_WORD_LENGTH; ++j) {
      words[l++] = henyoState.words[i][j];
      if (henyoState.words[i][j] == '\0') {
        words[l - 1] = '\n';
        break;
      }
    }
  }
  words[l] = '\0';

  AsyncWebServerResponse *resp = req->beginResponse(200, "text/plain", words);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void state_handler(AsyncWebServerRequest *req) {
  AsyncWebServerResponse *resp = req->beginResponse(200, "application/json", henyoState.getState());
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void body_handler(AsyncWebServerRequest *req, uint8_t *data, size_t len, size_t index, size_t total) {
  if (req->url() == "/henyo/start" && req->method() == HTTP_POST) {

    int t = data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24;
    henyoState.start(t);

    AsyncWebServerResponse *resp = req->beginResponse(204);
    resp->addHeader("Access-Control-Allow-Origin", "*");
    req->send(resp);
  } else if (req->url() == "/henyo/words" && req->method() == HTTP_POST) {
    static char buffer[1024];

    if (index == 0) {
      henyoState.wordCount = 0;
      henyoState.words[0] = buffer;
    }

    for (size_t i = 0; i < len; ++i) {
      size_t id = min(index + i, (size_t)1023);
      buffer[id] = data[i];
      if (data[i] == '\n') {
        buffer[id] = '\0';
        henyoState.wordCount++;
        if (henyoState.wordCount < WORD_COUNT)
          henyoState.words[henyoState.wordCount] = &buffer[id + 1];
      }
    }

    if (index + len == total) {
      AsyncWebServerResponse *resp = req->beginResponse(204);
      resp->addHeader("Access-Control-Allow-Origin", "*");
      req->send(resp);
    }
  }
}

void pause_handler(AsyncWebServerRequest *req) {
  henyoState.pause();
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void reset_handler(AsyncWebServerRequest *req) {
  henyoState.reset();
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void next_handler(AsyncWebServerRequest *req) {
  henyoState.next();
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void check_handler(AsyncWebServerRequest *req) {
  henyoState.check();
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void mode_handler(AsyncWebServerRequest *req) {
  AsyncWebServerResponse *resp = req->beginResponse(204);
  resp->addHeader("Access-Control-Allow-Origin", "*");
  req->send(resp);
}

void padnum(char *out, int x, int len) {
  for (int i = 0; i < len; ++i, x /= 10) {
    out[len - i] = x % 10;
  }
  out[len] = '\0';
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  randomSeed(analogRead(0));

  connected = false;
  int retries = 10;

#ifdef SOFTAP
  WiFi.softAP(SSID, PASSWORD);

  ip = WiFi.softAPIP();
  connected = true;
  Serial.print("AP IP address: ");
  Serial.println(ip);
#else
  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED && retries--) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
    connected = true;
  }

  ip = WiFi.localIP();
  if (connected) {
    Serial.print("IP address: ");
    Serial.println(ip);
  }
#endif

  if (connected) {
    server.on("*", HTTP_OPTIONS, cors_handler);
    server.on("/connect", HTTP_POST, connect_handler);
    server.on("/config", HTTP_GET, config_handler);
    server.on("/mode", HTTP_GET, mode_handler);
    server.on("/henyo/words", HTTP_GET, words_get_handler);
    server.on("/henyo/state", HTTP_GET, state_handler);
    server.on("/henyo/pause", HTTP_GET, pause_handler);
    server.on("/henyo/reset", HTTP_GET, reset_handler);
    server.on("/henyo/next", HTTP_GET, next_handler);
    server.on("/henyo/check", HTTP_GET, check_handler);

    server.onRequestBody(body_handler);


    server.begin();
  } else {
    Serial.println("Failed to connect");
    henyoState.sampleFromDefault();
  }

  lcd.init();
  lcd.clear();
  lcd.backlight();
}

void loop() {
  henyoState.update();
  delay(100);

  char t[3];
  lcd.clear();

  switch (henyoState.state) {
    default:
    case HENYO_IDLE:
      lcd.setCursor(2, 0);
      lcd.print("Pinoy Henyo!");

      if (connected) {
        lcd.setCursor(0, 1);
        lcd.print(ip);
      }
      break;
    case HENYO_ACTIVE:
      lcd.setCursor(0, 0);
      lcd.print(henyoState.words[henyoState.currentWord]);
      lcd.setCursor(0, 1);
      lcd.print(henyoState.score);

      padnum(t, henyoState.time / 60, 2);
      lcd.setCursor(11, 1);
      lcd.print(t);

      lcd.setCursor(13, 1);
      lcd.print(':');

      padnum(t, henyoState.time % 60, 2);
      lcd.setCursor(14, 1);
      lcd.print(t);
      break;
    case HENYO_PAUSED:
      lcd.setCursor(4, 0);
      lcd.print("PAUSED!!");
      lcd.setCursor(0, 1);
      lcd.print(henyoState.score);

      padnum(t, henyoState.time / 60, 2);
      lcd.setCursor(11, 1);
      lcd.print(t);

      lcd.setCursor(13, 1);
      lcd.print(':');

      padnum(t, henyoState.time % 60, 2);
      lcd.setCursor(14, 1);
      lcd.print(t);
      break;
  }

  delay(100); 
}