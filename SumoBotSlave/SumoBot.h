#pragma once

enum SumoState {
  SB_STOP,
  SB_START,
  SB_BEGIN,
  SB_SEARCH,
  SB_ATTACK,
  SB_DEFEND,
  SB_EVADE,
};

struct SumoSensors {
  unsigned int leftDist;
  unsigned int rightDist;
  unsigned int frontDist;
  unsigned int backDist;

  bool edgeSensor[4];
};

struct SumoConfig {
  // General Config
  unsigned char lrBias;
  unsigned int startDelay;

  // Defense Config
  unsigned int sensorDecay;
  unsigned int defendDuration;

  // Start Config
  int leftBegin;
  int rightBegin;
  unsigned int positioningTime;
  unsigned int scanningTime;

  // Evade Config
  unsigned int evadeDistance;

  SumoConfig();
};

class SumoBot {
private:
  void stayInCircle(const unsigned char state, int &outLeft, int &outRight, int depth);
  void defend(int &outLeft, int &outRight);
  void evade(int &outLeft, int &outRight);
  void begin(int &outLeft, int &outRight);
  bool search(int &outLeft, int &outRight);

public:
  SumoState state;
  SumoConfig config;
  SumoSensors sensor;

  unsigned long started;
  unsigned long defendingStart;

  unsigned long lastActivated[4];
  unsigned long lastDetected[4];
  unsigned long lastInDanger;
  unsigned char prevDefendState;
  unsigned char activeSensor;

  SumoBot();
  SumoBot(const SumoConfig &config);

  void start();
  void update(int &outLeft, int &outRight);
  void stop();
};