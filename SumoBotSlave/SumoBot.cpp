#include "Arduino.h"
#include "SumoBot.h"

#define SB_NW 0b0001
#define SB_NE 0b0010
#define SB_SE 0b0100
#define SB_SW 0b1000

#define SB_NW_I 0
#define SB_NE_I 1
#define SB_SE_I 2
#define SB_SW_I 3

#define MAX_RECURSION_DEPTH 8

SumoConfig::SumoConfig()
  : lrBias(127),
    startDelay(3000),
    sensorDecay(250),
    defendDuration(500),
    leftBegin(-127),
    rightBegin(-255),
    positioningTime(500),
    scanningTime(500) {}

SumoBot::SumoBot(const SumoConfig &_config)
  : state(SB_STOP), config(_config), started(0) {
}

SumoBot::SumoBot()
  : SumoBot(SumoConfig()) {}

void SumoBot::start() {
  state = SB_START;
  started = millis();
}

void SumoBot::stop() {
  state = SB_STOP;
}

void SumoBot::stayInCircle(unsigned char state, int &left, int &right, int depth) {
  unsigned long current = millis();
  switch (state) {
    default:
    case 0:
      if (current - lastInDanger >= config.defendDuration) {
        state = SB_SEARCH;
      } else if (depth < MAX_RECURSION_DEPTH) {
        stayInCircle(prevDefendState, left, right, depth + 1);
      } else {
        left = 255;
        right = -255;
      }
      break;

    case SB_NW:
    case SB_SW | SB_NE | SB_NW:
      // REVERSE RIGHT
      left = -255;
      right = -127;
      break;
    case SB_NE:
    case SB_SE | SB_NE | SB_NW:
      // REVERSE LEFT
      left = -127;
      right = -255;
      break;
    case SB_SE:
    case SB_SW | SB_SE | SB_NE:
      // FORWARD LEFT
      left = 127;
      right = 255;
      break;
    case SB_SW:
    case SB_SW | SB_SE | SB_NW:
      // FORWARD RIGHT
      left = 255;
      right = 127;
      break;


    case SB_NE | SB_NW:
      // REVERSE
      if (lastActivated[SB_NW_I] == lastActivated[SB_NE_I]) {
        left = right = -255;
      } else if (lastActivated[SB_NW_I] < lastActivated[SB_NE_I]) {
        left = -255;
        right = -127;
      } else {
        right = -127;
        left = -255;
      }
      break;


    case SB_SW | SB_SE:
      if (lastActivated[SB_SW_I] == lastActivated[SB_SE_I]) {
        left = right = 255;
      } else if (lastActivated[SB_SW_I] < lastActivated[SB_SE_I]) {
        left = 255;
        right = 127;
      } else {
        right = 127;
        left = 255;
      }
      break;


    case SB_SE | SB_NE:
      left = 127;
      right = 255;
      break;
    case SB_SW | SB_NW:
      left = 255;
      right = 127;
      break;


    case SB_SE | SB_NW:
      if (depth < MAX_RECURSION_DEPTH) {
        if (lastActivated[SB_NE_I] > lastActivated[SB_SW_I]) {
          stayInCircle(state | SB_NE, left, right, depth + 1);
        } else {
          stayInCircle(state | SB_SW, left, right, depth + 1);
        }
      } else {
        left = 255;
        right = -255;
      }
      break;

    case SB_SW | SB_NE:
      if (depth < MAX_RECURSION_DEPTH) {
        if (lastActivated[SB_NW_I] > lastActivated[SB_SE_I]) {
          stayInCircle(state | SB_NW, left, right, depth + 1);
        } else {
          stayInCircle(state | SB_SE, left, right, depth + 1);
        }
      } else {
        left = 255;
        right = -255;
      }
      break;


    case SB_SW | SB_SE | SB_NE | SB_NW:
      if (depth < MAX_RECURSION_DEPTH) {
        int min = lastActivated[0];
        int target = 1;
        for (int i = 1; i < 4; ++i) {
          if (lastActivated[i] < min) {
            target = 1 << i;
            min = lastActivated[i];
          } else if (lastActivated[i] == min) {
            target |= 1 << i;
          }
        }
      } else {
        left = 255;
        right = -255;
      }
      break;
  }
}

void SumoBot::defend(int &outLeft, int &outRight) {
  unsigned long current = millis();
  for (int i = 0; i < 4; ++i) {
    if (sensor.edgeSensor[i]) {
      if (activeSensor & (1 << i) != 0) {
        activeSensor |= 1 << i;
        lastActivated[i] = current;
      }
      lastDetected[i] = current;
    }

    if (current - lastDetected[i] >= config.sensorDecay) {
      activeSensor &= ~(1 << i);
    }
  }

  if (activeSensor) {
    state = SB_DEFEND;
    prevDefendState = state;
    lastInDanger = current;
  }
}

void SumoBot::begin(int &outLeft, int &outRight) {
  int current = millis() - started;
  if (current < config.startDelay) { return; }

  outLeft = config.leftBegin;
  outRight = config.rightBegin;
}

void SumoBot::evade(int &left, int &right) {
  if (state == SB_DEFEND) return;

  unsigned long latestDetected = lastDetected[0];
  int idx = 0;
  for (int i = 1; i < 4; ++i) {
    if (lastDetected[i] > latestDetected) {
      latestDetected = lastDetected[i];
      idx = i;
    }
  }

  if (sensor.backDist <= config.evadeDistance
      && sensor.backDist <= sensor.leftDist
      && sensor.backDist <= sensor.rightDist) {
    if (idx == SB_NE_I || idx == SB_SE_I) {
      // REVERSE RIGHT
      left = -255;
      right = -127;
    } else {
      // REVERSE LEFT
      left = -127;
      right = -255;
    }
  } else if (sensor.leftDist <= config.evadeDistance
               && sensor.leftDist < sensor.backDist
             || sensor.rightDist <= sensor.backDist
                  && sensor.rightDist < sensor.backDist) {
    if (idx == SB_NW_I || idx == SB_NE_I) {
      // REVERSE
      left = -255;
      right = -255;
    } else {
      // BACKWARD
      left = 255;
      right = 255;
    }
  }
}

void SumoBot::update(int &outLeft, int &outRight) {
  int current = millis();

  defend(outLeft, outRight);
  evade(outLeft, outRight);

  switch (state) {
    default:
    case SB_STOP:
      outLeft = outRight = 0;
      break;
    case SB_START:
      outLeft = outRight = 0;
      if (current - started >= config.startDelay) state = SB_BEGIN;
      break;
    case SB_BEGIN:
      begin(outLeft, outRight);
      break;
    case SB_SEARCH:
      search(outLeft, outRight);
      break;
    case SB_DEFEND:
    case SB_EVADE:
      break;
  }
}

#undef MAX_RECURSION_DEPTH
#undef SB_NW
#undef SB_NE
#undef SB_SE
#undef SB_SW
#undef SB_NW_I
#undef SB_NE_I
#undef SB_SE_I
#undef SB_SW_I