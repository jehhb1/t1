import { Background } from "@/components/Background";
import { ConfigContext } from "@/components/ConfigContext";
import { ConnectionContext } from "@/components/ConnectionContext";
import { router } from "expo-router";
import { useCallback, useContext, useEffect } from "react";
import { ActivityIndicator } from "react-native-paper";

export default function index() {
  const { fetch } = useContext(ConnectionContext);
  const config = useContext(ConfigContext);

  const redirect = useCallback(
    (v: string) => {
      fetch("mode?c=" + v).then((res) => {
        if (!res.ok) return;

        if (v == "rc") router.replace("/rc");
        else if (v == "sumo") router.replace("/sumo");
        else if (v == "henyo") router.replace("/henyo");
      });
    },
    [fetch],
  );

  useEffect(() => {
    if (config === null) return;

    if (config.rc !== undefined && config.rc !== false) {
      redirect("rc");
    } else if (config.sumo !== undefined && config.sumo !== false) {
      redirect("sumo");
    } else if (config.henyo !== undefined && config.henyo !== false) {
      redirect("henyo");
    }
  }, [config]);

  return (
    <Background className="flex flex-1 items-center justify-center">
      <ActivityIndicator size="large" />
    </Background>
  );
}
