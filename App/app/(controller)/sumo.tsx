import { Background } from "@/components/Background";
import { ControlSelection } from "@/components/ControlSellection";
import { Text } from "react-native-paper";

export default function sumo() {
  return (
    <Background className="flex flex-1">
      <ControlSelection state="sumo" />
      <Text>Sumo</Text>
    </Background>
  );
}
