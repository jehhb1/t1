import { ConfigProvider } from "@/components/ConfigContext";
import { ConnectionContext } from "@/components/ConnectionContext";
import { Redirect, Stack } from "expo-router";
import { useContext } from "react";

export default function controller() {
  const { conn } = useContext(ConnectionContext);

  if (!conn) {
    return <Redirect href="/connect" />;
  }

  return (
    <ConfigProvider>
      <Stack screenOptions={{ headerShown: false }} />
    </ConfigProvider>
  );
}
