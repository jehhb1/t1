import { ConnectionProvider } from "@/components/ConnectionContext";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import { useFonts } from "expo-font";
import { Slot } from "expo-router";
import * as SplashScreen from "expo-splash-screen";
import { useEffect } from "react";
import { MD3DarkTheme, PaperProvider } from "react-native-paper";
import { NativeWindStyleSheet } from "nativewind";

export { ErrorBoundary } from "expo-router";

export const unstable_settings = {
  initialRouteName: "(controller)",
};

SplashScreen.preventAutoHideAsync();

NativeWindStyleSheet.setOutput({
  default: "native",
});

export default function RootLayout() {
  const [loaded, error] = useFonts({
    SpaceMono: require("../assets/fonts/SpaceMono-Regular.ttf"),
    ...FontAwesome.font,
  });

  useEffect(() => {
    if (error) throw error;
  }, [error]);

  useEffect(() => {
    if (loaded) {
      SplashScreen.hideAsync();
    }
  }, [loaded]);

  if (!loaded) {
    return null;
  }

  return <RootLayoutNav />;
}

export const theme = MD3DarkTheme;

function RootLayoutNav() {
  return (
    <ConnectionProvider>
      <PaperProvider theme={theme}>
        <Slot />
      </PaperProvider>
    </ConnectionProvider>
  );
}
