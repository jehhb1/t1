import { Background } from "@/components/Background";
import { ConnectionContext } from "@/components/ConnectionContext";
import { router } from "expo-router";
import { useCallback, useContext, useRef, useState } from "react";
import { TextInput as _TextInput, View } from "react-native";
import { Button, Snackbar, Text, TextInput } from "react-native-paper";

export default function index() {
  const { setConn } = useContext(ConnectionContext);
  const [url, setUrl] = useState("http://192.168.4.1");
  const [password, setPassword] = useState("");
  const [error, showError] = useState<string | false>(false);

  const urlRef = useRef<_TextInput>(null);
  const passwordRef = useRef<_TextInput>(null);

  const connect = useCallback(
    function () {
      const endpoint = url.trim();
      if (endpoint.length === 0) {
        showError("Url cannot be empty");
        urlRef.current?.focus();
        return;
      }
      fetch(endpoint + "/connect", {
        method: "POST",
        headers: { "Content-Type": "text/plain" },
        body: password,
      })
        .then((res) => {
          if (!res.ok) {
            throw "Uknown error encountered";
          }

          return res.json();
        })
        .then((json) => {
          if (json["s"] == 1) {
            throw "Incorrect password";
          }
          if (json["s"] != 0) {
            throw "Incorrect password";
          }

          setConn({
            url: endpoint,
            session: json["t"],
          });
          router.replace("/");
        })
        .catch((error) => {
          if (error instanceof Error) {
            showError(error.message);
          } else if (typeof error === "string") {
            showError(error);
          } else {
            showError("Unkown error encountered");
          }

          if (error == "Incorrect password") {
            passwordRef.current?.focus();
          } else {
            urlRef.current?.focus();
          }
        });
    },
    [url, password],
  );

  return (
    <Background className="flex flex-1 items-center justify-center">
      <View className="flex w-80">
        <Text className="mb-4 self-center font-bold" variant="headlineLarge">
          Connect Bot!!
        </Text>
        <View className="mb-8 flex gap-2">
          <TextInput
            ref={urlRef}
            label="URL"
            mode="outlined"
            placeholder="http://192.168.4.1"
            keyboardType="url"
            textContentType="URL"
            returnKeyType="next"
            onChangeText={(url) => setUrl(url)}
            onSubmitEditing={() => passwordRef.current?.focus()}
            value={url}
            blurOnSubmit={false}
          />
          <TextInput
            ref={passwordRef}
            label="Password"
            mode="outlined"
            placeholder="Leave blank if none"
            returnKeyType="go"
            textContentType="password"
            onChangeText={(password) => setPassword(password)}
            onSubmitEditing={connect}
            value={password}
            secureTextEntry
          />
        </View>
        <Button mode="contained" onPress={connect}>
          Connect
        </Button>
      </View>
      <Snackbar
        visible={error !== false}
        onDismiss={() => showError(false)}
        action={{ label: "dismiss", onPress: () => showError(false) }}
      >
        {error}
      </Snackbar>
    </Background>
  );
}
