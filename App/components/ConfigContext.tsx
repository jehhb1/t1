import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import { ConnectionContext } from "./ConnectionContext";

export type Config = {
  rc?: true | false | {};
  sumo?: true | false;
  henyo?: true | false;
};

export const ConfigContext = createContext<Config | null>(null);

export function ConfigProvider({ children }: { children: ReactNode }) {
  const [config, setConfig] = useState<Config | null>(null);
  const { conn, fetch } = useContext(ConnectionContext);

  useEffect(() => {
    if (conn == null) {
      setConfig(null);
    } else {
      fetch("config")
        .then((res) => res.json())
        .then((res: Config) => {
          setConfig(res);
        });
    }
  }, [conn]);

  return <ConfigContext.Provider value={config} children={children} />;
}
