import { Button, IconButton, Text, TextInput } from "react-native-paper";
import { Background } from "./Background";
import { ControlSelection } from "./ControlSellection";
import { View } from "react-native";
import { useCallback, useContext, useEffect, useState } from "react";
import shuffle from "lodash/shuffle";
import sampleSize from "lodash/sampleSize";
import clamp from "lodash/clamp";
import { ConnectionContext } from "./ConnectionContext";
import throttle from "lodash/throttle";

// prettier-ignore
const pinoyHenyoWords = [
    "bahay", "sapatos", "telepono", "aso", "pusa", "pagkain", "puno", "tubig", 
    "ulan", "init", "lamig", "hangin", "kabayo", "isda", "karne", "gatas", 
    "gulay", "prutas", "balita", "libro", "kompyuter", "barko", "eroplano", 
    "bisikleta", "kotse", "motor", "jeep", "bus", "kalye", "kalsada", "pulis", 
    "bumbero", "hospital", "doktor", "nars", "guro", "eskwela", "bata", 
    "matanda", "binata", "dalaga", "kaibigan", "kapatid", "tatay", "nanay", 
    "lolo", "lola", "anak", "daga", "ibon", "butterfly", "buwan", "araw", 
    "bituin", "lupa", "dagat", "bundok", "ilog", "sapa", "lawa", "kubo", 
    "kusina", "sala", "kwarto", "banyo", "bintana", "pintuan", "hagdan", 
    "mesa", "upuan", "kama", "orasan", "alarma", "bumbilya", "kandila", 
    "kabinet", "aparador", "plato", "kutsara", "tinidor", "kutsilyo", "baso", 
    "takure", "kaldero", "kawali", "sandok", "palayok", "banga", "plangana", 
    "tabo", "timbangan", "plantsa", "kutsarita", "kalendaryo", "payong", 
    "kwintas", "singsing", "hikaw", "pulseras", "relos"
];

function processWordList(wordlist: string) {
  return wordlist
    .split("\n")
    .map((s) => s.trim().toLowerCase())
    .filter((s) => s !== "");
}

function safeParseInt(x: string) {
  const number = Number.parseInt(x, 10);
  return Number.isNaN(number) ? 0 : number;
}

type HenyoState = "active" | "paused" | "idle";

type FetchState = { s: 0 } | { s: 1 | 2; t: number; w: string; sc: number };

export function Henyo() {
  const { fetch } = useContext(ConnectionContext);
  const [wordList, setWordList] = useState("");
  const [time, setTime] = useState({ m: 2, s: 0 });
  const [timer, setTimer] = useState({ m: 0, s: 0 });
  const [state, setState] = useState<HenyoState>("idle");
  const [currentWord, setCurrentWord] = useState("");
  const [score, setScore] = useState(0);

  useEffect(() => {
    fetch("henyo/words")
      .then((res) => res.text())
      .then((words) => setWordList(words));
  }, [fetch]);

  useEffect(() => {
    const active = { a: true };

    const getState = throttle(async function getState() {
      try {
        const state = await fetch("henyo/state");

        const data = (await state.json()) as FetchState;
        if (data.s == 0) {
          setState("idle");
          return;
        }

        setState(data.s == 1 ? "active" : "paused");
        const time = { m: Math.floor(data.t / 60), s: data.t % 60 };
        setTimer(time);
        setScore(data.sc);
        setCurrentWord(data.w);
      } catch (e) {
        console.log(e);
        return;
      }
    }, 100);

    async function updateState() {
      while (active.a) {
        await getState();
      }
    }
    updateState();
    return () => {
      active.a = false;
    };
  }, [fetch]);

  const start = useCallback(
    async function start() {
      await fetch("henyo/words", {
        method: "POST",
        body: wordList.trim() + "\n",
      });

      const t = time.m * 60 + time.s;
      const timeBytes = new Uint8Array([
        (t >> 0) & 255,
        (t >> 8) & 255,
        (t >> 16) & 255,
        (t >> 24) & 255,
      ]);

      await fetch("henyo/start", {
        method: "POST",
        body: timeBytes,
        headers: { "Content-Type": "application/octet-stream" },
      });
    },
    [fetch, wordList, time],
  );

  return (
    <Background className="flex flex-1">
      <ControlSelection state="henyo" />
      <View className="flex flex-1 flex-row gap-4 p-4">
        <View className="flex flex-1 gap-4">
          {state == "idle" ? (
            <>
              <Text variant="titleSmall" className="font-semibold">
                Wordlist
              </Text>
              <TextInput
                mode="outlined"
                className="h-full flex-1 overflow-clip"
                value={wordList}
                onChangeText={(s) => setWordList(s)}
                multiline
                contentStyle={{ height: "100%" }}
                readOnly={state !== "idle"}
                disabled={state !== "idle"}
                editable={state === "idle"}
              />
              <View className="flex flex-row gap-2">
                <IconButton
                  icon="dice-6"
                  mode="contained"
                  className="flex-1"
                  disabled={state !== "idle"}
                  onPress={() => {
                    const sample = sampleSize(pinoyHenyoWords, 20);
                    setWordList(sample.join("\n"));
                  }}
                />
                <IconButton
                  icon="shuffle"
                  mode="contained"
                  className="flex-1"
                  disabled={state !== "idle"}
                  onPress={() => {
                    setWordList((w) => {
                      const words = processWordList(w);
                      return shuffle(words).join("\n");
                    });
                  }}
                />
                <IconButton
                  icon="delete-sweep"
                  mode="contained"
                  className="flex-1"
                  disabled={state !== "idle"}
                  onPress={() => setWordList("")}
                />
              </View>
            </>
          ) : (
            <>
              <Text variant="titleSmall" className="font-semibold">
                Current Word
              </Text>
              <Text variant="headlineLarge">{currentWord}</Text>
              <Text variant="titleSmall" className="font-semibold">
                Score
              </Text>
              <Text variant="headlineLarge">{score}</Text>
            </>
          )}
        </View>
        <View className="flex-1 gap-4">
          <Text variant="titleSmall" className="font-semibold">
            Time
          </Text>
          <View className="flex flex-row justify-center px-8">
            <TextInput
              mode="outlined"
              value={(state == "idle" ? time : timer).m
                .toString()
                .padStart(1, "0")}
              keyboardType="number-pad"
              inputMode="numeric"
              className="flex-1"
              readOnly={state != "idle"}
              contentStyle={{ padding: 0, textAlign: "center" }}
              onChangeText={(s) =>
                setTime((t) => ({ ...t, m: safeParseInt(s) }))
              }
              dense
            />
            <Text className="self-center px-1 text-3xl">:</Text>
            <TextInput
              mode="outlined"
              keyboardType="number-pad"
              value={(state == "idle" ? time : timer).s
                .toString()
                .padStart(2, "0")}
              inputMode="numeric"
              className="flex-1"
              readOnly={state != "idle"}
              onChangeText={(s) =>
                setTime((t) => ({ ...t, s: clamp(safeParseInt(s), 0, 59) }))
              }
              contentStyle={{ padding: 0, textAlign: "center" }}
              dense
            />
          </View>
          <View className="flex flex-row gap-2">
            {state == "idle" ? (
              <Button
                mode="contained"
                className="flex-1"
                onPress={() => start()}
              >
                Start
              </Button>
            ) : (
              <>
                <Button
                  mode="outlined"
                  className="flex-1"
                  onPress={() => fetch("henyo/pause")}
                >
                  {state == "active" ? "Pause" : "Resume"}
                </Button>
                <Button
                  mode="contained"
                  className="flex-1"
                  onPress={() => fetch("henyo/reset")}
                >
                  Reset
                </Button>
              </>
            )}
          </View>
          {state != "idle" &&
            (state == "paused" ? (
              <View className="flex flex-1 items-center justify-center">
                <Text variant="headlineLarge" className="text-center">
                  Paused
                </Text>
              </View>
            ) : (
              <View className="flex flex-1 flex-row items-stretch gap-2">
                <Button
                  className="flex flex-1 justify-center"
                  icon="close"
                  mode="outlined"
                  onPress={() => fetch("henyo/next")}
                >
                  Skip
                </Button>
                <Button
                  className="flex flex-1 justify-center"
                  icon="check"
                  mode="contained-tonal"
                  onPress={() => fetch("henyo/check")}
                >
                  Check
                </Button>
              </View>
            ))}
        </View>
      </View>
    </Background>
  );
}
