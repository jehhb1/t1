import { Background } from "@/components/Background";
import { ControlSelection } from "@/components/ControlSellection";
import { Joystick } from "@/components/Joystick";
import Slider from "@react-native-community/slider";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { View } from "react-native";
import {
  Button,
  Checkbox,
  Dialog,
  Divider,
  IconButton,
  Portal,
  Text,
  Tooltip,
  useTheme,
} from "react-native-paper";
import { ConnectionContext } from "./ConnectionContext";
import debounce from "lodash/debounce";

export type RCConfig = {
  lr: number;
  ms: number;
};

type FetchLike = (url: string, options?: RequestInit) => Promise<Response>;

class Mover {
  public lastPos: [number, number];
  private hasPending: boolean;

  constructor() {
    this.lastPos = [0.0, 0.0];
    this.hasPending = false;
  }

  public move(pos: [number, number], fetch: FetchLike, curve: boolean) {
    if (this.hasPending) return;

    const [x1, y1] = this.lastPos;
    const [x, y] = pos;
    if (x1 == 0 && y1 == 0 && x == 0 && y == 0) return;

    let mag = x * x + y * y;
    if (!curve) mag = Math.sqrt(mag);
    mag = Math.floor(mag * 255);

    let angle = Math.floor(
      ((Math.atan2(-y, -x) + Math.PI) / (Math.PI * 2)) * 255,
    );

    const data = new Uint8Array([0, mag, angle]);
    const abort = new AbortController();
    setTimeout(() => abort.abort("Request timeout"), 250);

    this.hasPending = true;
    fetch("rc", {
      method: "POST",
      headers: { "Content-Type": "application/octet-stream" },
      body: data,
      signal: abort.signal,
    })
      .then(() => {
        this.lastPos = pos;
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        this.hasPending = false;
      });
  }
}

/** temporary fix for a react native slider bug */
function __fix(a: number): number {
  const DELTA = 0.0000001;
  return a > DELTA || a < -DELTA ? a : DELTA;
}

const mover = new Mover();

export function RC() {
  const theme = useTheme();
  const [config, setConfig] = useState<RCConfig>({
    lr: 0,
    ms: 1,
  });
  const [curve, setCurve] = useState(true);
  const { fetch } = useContext(ConnectionContext);

  const [calVisible, setCalVisible] = useState(false);
  const [forward, setForward] = useState(false);
  const [calibrating, setCalibrating] = useState(false);

  const updateConfig = useCallback(
    debounce((c: RCConfig) => {
      const lr = Math.floor(((c.lr + 1) / 2) * 255);
      const ms = Math.floor(c.ms * 255);

      fetch("rc", {
        method: "POST",
        headers: { "Content-Type": "application/octet-stream" },
        body: new Uint8Array([1, lr, ms]),
      });
    }, 500),
    [fetch],
  );

  useEffect(() => {
    const cont = { current: forward };

    async function _forward() {
      if (!cont.current) return;

      const res = await fetch("rc", {
        method: "POST",
        headers: { "Content-Type": "application/octet-stream" },
        body: new Uint8Array([3]),
      });
      if (res.ok) _forward();
    }
    _forward();

    return () => {
      cont.current = false;
    };
  }, [forward]);

  useEffect(() => {
    updateConfig(config);
  }, [config, updateConfig]);

  return (
    <Background className="flex flex-1">
      <ControlSelection state="rc" />
      <View className="flex-1 flex-row items-center justify-start">
        <Joystick
          disabled={forward || calibrating}
          update={(pos) => mover.move(pos, fetch, curve)}
        />
        <View className="mx-auto max-w-md flex-1">
          <View className="flex-row items-center justify-start gap-2">
            <Tooltip title="Control left and right steering bias">
              <Text>L/R Bias</Text>
            </Tooltip>
            <IconButton
              icon="minus"
              onPress={() =>
                setConfig((c) => ({ ...c, lr: Math.max(-1, c.lr - 0.05) }))
              }
            />
            <Slider
              style={{ flexGrow: 1 }}
              onValueChange={(v) => setConfig((c) => ({ ...c, lr: v }))}
              value={__fix(config.lr)}
              minimumValue={-1}
              maximumValue={1}
              step={0.01}
              thumbTintColor={theme.colors.primary}
              minimumTrackTintColor={theme.colors.elevation.level5}
              maximumTrackTintColor={theme.colors.elevation.level5}
            />
            <IconButton
              icon="plus"
              onPress={() =>
                setConfig((c) => ({ ...c, lr: Math.min(1, c.lr + 0.05) }))
              }
            />
            <Tooltip title="Calibrate steering bias">
              <IconButton
                onPress={() => setCalVisible(true)}
                icon="format-horizontal-align-center"
                accessibilityHint="reset steering bias"
                mode="contained"
              />
            </Tooltip>
          </View>
          <View className="flex-row items-center justify-start gap-2">
            <Tooltip title="Set maxspeed of the robot">
              <Text>Throttle</Text>
            </Tooltip>
            <IconButton
              icon="minus"
              onPress={() =>
                setConfig((c) => ({ ...c, ms: Math.max(0, c.ms - 0.05) }))
              }
            />
            <Slider
              style={{ flexGrow: 1 }}
              onValueChange={(v) => setConfig((c) => ({ ...c, ms: v }))}
              value={__fix(config.ms)}
              minimumValue={0}
              maximumValue={1}
              step={0.005}
              thumbTintColor={theme.colors.primary}
              minimumTrackTintColor={theme.colors.primary}
              maximumTrackTintColor={theme.colors.elevation.level5}
            />
            <IconButton
              icon="plus"
              onPress={() =>
                setConfig((c) => ({ ...c, ms: Math.min(1, c.ms + 0.05) }))
              }
            />
          </View>
          <View className="flex-row items-center justify-start gap-4">
            <View className="flex-row items-center justify-start gap-1">
              <Checkbox
                status={curve ? "checked" : "unchecked"}
                onPress={() => setCurve((c) => !c)}
              />
              <Text variant="labelMedium">Throttle curve</Text>
            </View>
          </View>
          <Divider className="my-4" />
          <View className="flex-row items-center justify-end gap-2">
            <Button
              mode="outlined"
              icon="restore"
              onPress={() => {
                setConfig({
                  lr: 0,
                  ms: 1,
                });
                setCurve(true);
              }}
            >
              Restore defaults
            </Button>
            <Button
              mode="contained"
              icon="chevron-triple-up"
              onPressIn={() => setForward(true)}
              onPressOut={() => setForward(false)}
              disabled={calibrating}
            >
              Max forward
            </Button>
          </View>
        </View>
      </View>
      <Portal>
        <Dialog
          visible={calVisible}
          onDismiss={() => setCalVisible(false)}
          style={{ maxWidth: 400, alignSelf: "center" }}
        >
          <Dialog.Title>Start calibration</Dialog.Title>
          <Dialog.Content>
            <Text variant="bodySmall">
              Place the robot in a long straight and flat path, then observe the
              bot trajectory. Adjust L/R bias until the bot follows a straight
              path.
            </Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => setCalVisible(false)}>Cancel</Button>
            <Button
              onPress={() => {
                setCalVisible(false);
                fetch("rc", {
                  method: "POST",
                  headers: { "Content-Type": "application/octet-stream" },
                  body: new Uint8Array([2]),
                }).then(() => {
                  setCalibrating(true);
                  setTimeout(() => {
                    setCalibrating(false);
                  }, 3000);
                });
              }}
            >
              Start
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Background>
  );
}
