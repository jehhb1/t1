import React, { createContext, useCallback, useState } from "react";

export type Connection = {
  url: string;
  session: string;
};

export const ConnectionContext = createContext<{
  conn: Connection | null;
  setConn: React.Dispatch<React.SetStateAction<Connection | null>>;
  fetch: (url: string, options?: RequestInit) => Promise<Response>;
}>({ conn: null, setConn() {}, fetch });

export function ConnectionProvider(props: { children: React.ReactNode }) {
  const [conn, setConn] = useState<Connection | null>(null);

  const _fetch = useCallback(
    (url: string, options?: RequestInit) => {
      if (!conn) return fetch(url, options);
      return fetch(conn.url + "/" + url, {
        ...options,
        headers: { ...options?.headers, "X-T1-Auth": conn.session },
      });
    },
    [conn],
  );

  return (
    <ConnectionContext.Provider
      value={{ conn, setConn, fetch: _fetch }}
      children={props.children}
    />
  );
}
