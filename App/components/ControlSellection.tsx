import { useContext } from "react";
import { IconButton, SegmentedButtons } from "react-native-paper";
import { ConfigContext } from "./ConfigContext";
import { router } from "expo-router";
import { View } from "react-native";
import { ConnectionContext } from "./ConnectionContext";

export type State = "rc" | "sumo" | "henyo";

export function ControlSelection({ state }: { state: State }) {
  const { conn, setConn, fetch } = useContext(ConnectionContext);
  const config = useContext(ConfigContext);

  function redirect(v: string) {
    if (!conn) return;
    fetch("mode?c=" + v).then((res) => {
      if (!res.ok) return;

      if (v == "rc") router.replace("/rc");
      else if (v == "sumo") router.replace("/sumo");
      else if (v == "henyo") router.replace("/henyo");
    });
  }

  return (
    <View className="my-4 flex-row items-center self-center">
      <SegmentedButtons
        value={state}
        onValueChange={redirect}
        style={{ width: 480, marginEnd: 8 }}
        buttons={[
          {
            value: "rc",
            label: "RC",
            icon: "controller-classic",
            disabled:
              config === null || config.rc === undefined || config.rc == false,
          },
          {
            value: "sumo",
            label: "Sumo",
            icon: "bulldozer",
            disabled:
              config === null ||
              config.sumo === undefined ||
              config.sumo == false,
          },
          {
            value: "henyo",
            label: "Pinoy Henyo",
            icon: "racing-helmet",
            disabled:
              config === null ||
              config.henyo === undefined ||
              config.henyo == false,
          },
        ]}
        density="small"
      />
      <IconButton
        mode="contained-tonal"
        icon="logout"
        size={16}
        onPress={() => setConn(null)}
        style={{ margin: 0, height: 34, width: 34 }}
      />
    </View>
  );
}
