import React, { useCallback, useEffect, useRef, useState } from "react";
import { GestureResponderEvent, View } from "react-native";
import { GLView, ExpoWebGLRenderingContext } from "expo-gl";

function clamp(pos: [number, number]): [number, number] {
  const [x, y] = pos;
  const d2 = x * x + y * y;
  if (d2 < 1.0) return [...pos];
  const d = Math.sqrt(d2);
  return [x / d, y / d];
}

class Circle {
  gl: ExpoWebGLRenderingContext;
  segments: number;
  vbo: WebGLBuffer;

  constructor(gl: ExpoWebGLRenderingContext, segments = 30) {
    this.gl = gl;
    this.segments = segments;

    const vbo = gl.createBuffer();
    if (vbo === null) throw Error("Cannot create vbo");
    this.vbo = vbo;

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    const points = new Float32Array(segments * 2);
    for (let i = 0; i < segments; ++i) {
      points[i * 2] = Math.sin((i * 2 * Math.PI) / segments);
      points[i * 2 + 1] = Math.cos((i * 2 * Math.PI) / segments);
    }
    gl.bufferData(gl.ARRAY_BUFFER, points, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
  }

  draw(
    program: WebGLProgram,
    attr?: {
      radius?: number;
      center?: [number, number];
      color?: [number, number, number];
    },
  ) {
    if (attr == undefined) attr = {};
    if (attr.radius == undefined) attr.radius = 1.0;
    if (attr.center == undefined) attr.center = [0.0, 0.0];
    if (attr.color == undefined) attr.color = [1.0, 1.0, 1.0];

    this.gl.useProgram(program);
    const tl = this.gl.getUniformLocation(program, "transform");
    if (tl) {
      // prettier-ignore
      this.gl.uniformMatrix4fv(tl, false, [
        attr.radius, 0.0, 0.0, 0.0,
        0.0, attr.radius, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        attr.center[0], attr.center[1], 0.0, 1.0,
      ]);
    }

    const cl = this.gl.getUniformLocation(program, "color");
    if (cl) {
      this.gl.uniform3fv(cl, attr.color);
    }

    const pl = this.gl.getAttribLocation(program, "position");
    if (pl >= 0) {
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vbo);
      this.gl.vertexAttribPointer(pl, 2, this.gl.FLOAT, false, 0, 0);
      this.gl.enableVertexAttribArray(pl);
      this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, this.segments);
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
    }
  }
}

class GLJoystick {
  gl: ExpoWebGLRenderingContext;
  program: WebGLProgram;
  circle: Circle;

  constructor(gl: ExpoWebGLRenderingContext) {
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    gl.clearColor(0, 0, 0, 0);

    const vert = gl.createShader(gl.VERTEX_SHADER);
    if (vert == null) throw Error("shader creation error");
    gl.shaderSource(
      vert,
      `
      precision mediump float;

      attribute vec2 position;
      uniform mat4 transform;

      mat4 s = mat4(
          1.0/1.5, 0.0, 0.0, 0.0,
          0.0, 1.0/1.5, 0.0, 0.0,
          0.0, 0.0, 1.0, 0.0,
          0.0, 0.0, 0.0, 1.0
      );

      void main(void) {
        gl_Position = s * transform * vec4(position, 0.0, 1.0);
      }
      `,
    );
    gl.compileShader(vert);

    const frag = gl.createShader(gl.FRAGMENT_SHADER);
    if (frag == null) throw Error("shader creation error");
    gl.shaderSource(
      frag,
      `
      precision mediump float;
      uniform vec3 color;

      void main(void) {
        gl_FragColor = vec4(color, 1.0);
      }
      `,
    );
    gl.compileShader(frag);

    const program = gl.createProgram();
    if (program == null) throw Error("Program creation error");

    gl.attachShader(program, vert);
    gl.attachShader(program, frag);
    gl.linkProgram(program);
    gl.useProgram(program);
    gl.deleteShader(vert);
    gl.deleteShader(frag);

    this.gl = gl;
    this.program = program;
    this.circle = new Circle(gl, 36);
  }

  draw({
    pos,
    active,
    disabled,
  }: {
    pos?: [number, number];
    active?: boolean;
    disabled?: boolean;
  }) {
    if (pos == undefined) pos = [0.0, 0.0];
    if (disabled == undefined) disabled = false;

    this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    this.circle.draw(this.program, {
      color: [0.19, 0.19, 0.2],
    });
    this.circle.draw(this.program, {
      radius: 0.4,
      center: disabled ? [0.0, 0.0] : pos,
      color: disabled
        ? [0.47, 0.46, 0.47]
        : active
          ? [0.4, 0.32, 0.64]
          : [0.37, 0.36, 0.44],
    });
    this.gl.flush();
    this.gl.endFrameEXP();
  }
}

export function Joystick({
  size,
  disabled,
  update,
}: {
  size?: number;
  disabled?: boolean;
  update?: (pos: [number, number]) => void;
}) {
  if (size === undefined) size = 200;
  if (disabled === undefined) disabled = false;
  if (update === undefined) update = () => {};

  const [pos, setPos] = useState<[number, number]>([0, 0]);
  const [offset, setOffset] = useState<[number, number]>([0, 0]);
  const [joystick, setJoystick] = useState<GLJoystick | null>(null);
  const [active, setActive] = useState(false);
  const [touchid, setTouchid] = useState<string | null>(null);

  const view = useRef<View | null>(null);

  const respondGesture = useCallback(
    (ev: GestureResponderEvent) => {
      if (disabled) return;
      if (view.current == null) return;
      if (ev.nativeEvent.identifier !== touchid) return;

      setPos(
        clamp([
          (2 * (ev.nativeEvent.pageX - offset[0])) / size - 1.5,
          (-2 * (ev.nativeEvent.pageY - offset[1])) / size + 1.5,
        ]),
      );
    },
    [offset, size, disabled],
  );

  useEffect(() => {
    joystick?.draw({ pos, active, disabled });
  }, [joystick, pos, active, disabled]);

  const posRef = useRef<[number, number]>([0.0, 0.0]);

  useEffect(() => {
    posRef.current = [...pos];
  }, [pos]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (!disabled) update(posRef.current);
    }, 20);

    return () => clearInterval(interval);
  }, [update, disabled]);

  return (
    <View
      ref={view}
      onStartShouldSetResponder={() => true}
      onMoveShouldSetResponderCapture={() => true}
      onResponderStart={(ev) => {
        ev.persist();
        view.current?.measure((_x, _y, _w, _h, px, py) => setOffset([px, py]));
        setActive(true);
        setTouchid((prev) => (prev == null ? ev.nativeEvent.identifier : prev));
        respondGesture(ev);
      }}
      onResponderMove={respondGesture}
      onResponderRelease={() => {
        setPos([0, 0]);
        setTouchid(null);
        setActive(false);
      }}
    >
      <GLView
        style={{ width: size * 1.5, height: size * 1.5 }}
        onContextCreate={(gl) => setJoystick(new GLJoystick(gl))}
      />
    </View>
  );
}
